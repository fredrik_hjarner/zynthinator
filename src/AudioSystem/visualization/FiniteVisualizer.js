import { audioUtility } from '../audioUtility';

/**
 * Vizualizer class
 *
 * I want to have a Vizualizer class that can
 * visualize an array of floats with values 0 to 1.
 *
 * Thus I can for example visualize my ADSR node's
 * data.
 *
 * I want one to visualize a finite sound,
 * and one for continuous visualization of
 * (infinite) sounds.
 */

export class FiniteVisualizer {
  /**
   * Tried to improve the function above.
   * 3.54 milliseconds.
   * @param {*} float32Array
   * @param {*} canvasId
   */
  static visualizePerPixel(float32Array, canvasId) {
    const canvas = document.getElementById(canvasId);
    const canvasCtx = canvas.getContext('2d');
    canvasCtx.fillStyle = 'black';
    canvasCtx.fillRect(0, 0, canvas.width, canvas.height);
    const canvasData = canvasCtx.getImageData(0, 0, canvas.width, canvas.height);
    function drawPixel(x, y) {
      // let _x = Math.floor(x); // todo unoptimized!!!!
      const _y = Math.floor(y);
      const index = (x + (_y * canvas.width)) * 4;
      canvasData.data[index + 0] = 50;
      canvasData.data[index + 1] = 255;
      canvasData.data[index + 2] = 50;
      canvasData.data[index + 3] = 255;
    }
    let x = 0;
    for (; x < canvas.width; x++) {
      const indexInArray = Math.round(float32Array.length * x / canvas.width);
      const v = float32Array[indexInArray];
      const y = (-v * (canvas.height / 2))
              + (canvas.height / 2);
      if (x === 0) {
        drawPixel(x, y); // todo unoptimized to check this if in each iteration.
      } else {
        drawPixel(x, y);
      }
    }
    const indexInArray = Math.round(float32Array.length * x / canvas.width);
    const v = float32Array[indexInArray];
    const y = (-v * canvas.height / 2)
            + (canvas.height / 2);
    drawPixel(x, y);
    canvasCtx.putImageData(canvasData, 0, 0);
  }

  static visualizePerPixelFrequencyDomain(float32Array, canvasId) {
    const canvas = document.getElementById(canvasId);
    const canvasCtx = canvas.getContext('2d');
    // drawVisual = requestAnimationFrame(draw);
    canvasCtx.fillStyle = 'rgba(0, 0, 0, 255)';
    canvasCtx.fillRect(0, 0, canvas.width, canvas.height);
    const canvasData = canvasCtx.getImageData(0, 0, canvas.width, canvas.height);
    function drawPixel(x, y) {
      const _x = Math.floor(x); // todo unoptimized!!!!
      const _y = Math.floor(y);
      const index = (_x + (_y * canvas.width)) * 4;
      canvasData.data[index + 0] = 255;
      canvasData.data[index + 1] = 0;
      canvasData.data[index + 2] = 0;
      canvasData.data[index + 3] = 255;
    }

    // const barWidth = (canvas.width / float32Array.length) * 1;
    // let barHeight;
    // let x = 0;

    const indexesPerPixel = float32Array.length / canvas.width;
    // let pixelCounter = 0;
    for (let x = 0; x < canvas.width; x++) {
      // canvasCtx.fillStyle = 'rgba(50, 255, 50, 255)';
      // canvasCtx.fillRect(x, canvas.height - barHeight, 1, 1);
      // x += barWidth;
      const i = Math.round(indexesPerPixel * x);
      const value = float32Array[i];
      let height = ((value + 55) * 0.0155);
      height = audioUtility.gainToLoudness(height);
      if (height > 1) {
        height = 1;
      }
      const y = Math.round(canvas.height - (height * canvas.height));
      for (let j = canvas.height; j >= y; j--) {
        drawPixel(x, j);
      }
    }
    canvasCtx.putImageData(canvasData, 0, 0);
  }
}
