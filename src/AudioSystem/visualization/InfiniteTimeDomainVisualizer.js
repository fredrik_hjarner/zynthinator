import { FiniteVisualizer } from './FiniteVisualizer';

/**
 * Vizualizer class
 *
 * I want to have a Vizualizer class that can
 * visualize an array of floats with values 0 to 1.
 *
 * Thus I can for example visualize my ADSR node's
 * data.
 *
 * I want one to visualize a finite sound,
 * and one for continuous visualization of
 * (infinite) sounds.
 */

export class InfiniteTimeDomainVisualizer {
  init(analyserNode, timeDomainCanvas) {
    this.analyserNode = analyserNode;
    // this.samplesToStore = samplesToStore;
    this.timeDomainCanvas = timeDomainCanvas;
    // this.frequencyDomainCanvas = frequencyDomainCanvas;
    // this.analyserNode.fftSize = samplesToStore;
    // const bufferLength = analyserNode.fftSize;
    this.timeDomainDataArray = new Float32Array(analyserNode.fftSize); // todo. one of the two should be twice as large !?!?!??!?
    // this.frequencyDomainDataArray = new Float32Array(analyserNode.frequencyBinCount); // todo. one of the two should be twice as large !?!?!??!?

    // frequency domain setup.
    /* this.analyserNode.minDecibels = -31;
    this.analyserNode.maxDecibels = 100; */
    // this.analyserNode.smoothingTimeConstant = 0;
  }
  setMillisecondsBetweenRedraws(millisecondsBetweenRedraws) {
    // stop interval if it is not undefined
    if (this.interval !== undefined) {
      clearInterval(this.interval);
    }

    // start a new interval with correct
    this.interval = setInterval(this.draw.bind(this), millisecondsBetweenRedraws);
  }
  draw() {
    // requestAnimationFrame(draw);
    this.analyserNode.getFloatTimeDomainData(this.timeDomainDataArray);
    // this.analyserNode.getFloatFrequencyData(this.frequencyDomainDataArray);
    FiniteVisualizer.visualizePerPixel(this.timeDomainDataArray, this.timeDomainCanvas);
    // FiniteVisualizer.visualizePerPixelFrequencyDomain(this.frequencyDomainDataArray, this.frequencyDomainCanvas);
    // setTimeout(draw, document.interval);
    // console.log(Date.now());
  }
  destructor =
    () => {
      clearInterval(this.interval);
    }
}
