import _ from 'lodash';
import deepFreeze from 'deep-freeze';
import {
  routerReducer,
} from 'react-router-redux';
import { initialState } from '../InitialState';
import * as Reducers from './subReducers';
/* import {
  reduxHistory,
} from '../ReduxHistory'; */

/**
 * string -> string
 * CLICK_TRIGGER -> clickTriggerReducer
 */
const actionConstToReducer =
  actionConst =>
    _.camelCase(`${actionConst}Reducer`);
    

const _rootReducer =
  (_state, action) => {
    const state = {
      ..._state,
      router:
        routerReducer(_state.router, action),
    };
    const {
      type,
    } = action;

    if (type.startsWith('@@')) {
      return state;
    }

    const reducerName = actionConstToReducer(type);

    try {
      return Reducers[reducerName](state, action);
    } catch (exception) {
      alert(`action.type=${type}. Error with/in reducer with name '${reducerName}'.`);
      debugger;
      return state;
    }
  };

/**
 * Throw exception when trying to access a key that does not exist!
 */
const handler = {
  get(target, key) {
    if (key in target || key === 'toJSON' || key.startsWith('@@')) {
      return target[key];
    }
    debugger;
    throw `Exception: '${key}' property does not exist in state.`;
  },
};

export const rootReducer =
  (_state = initialState, action) => {
    let state = Reducers.addToHistoryReducer(_state, action);
    // reduxHistory.addActionToHistory(action);
    state =
      _rootReducer(state, action);
    deepFreeze(state);
    state = new Proxy(state, handler);
    return state;
  };
