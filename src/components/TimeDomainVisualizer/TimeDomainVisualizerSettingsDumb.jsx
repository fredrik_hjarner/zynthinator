import React from 'react';
import {
// Accordion,
// Icon,
} from 'semantic-ui-react';

export const TimeDomainVisualizerSettingsDumb =
  props => (
    props.settingsMinimized ?
      null :
      (
        <div>
          <div>
            <span>Milliseconds to record: </span>
            <input
              onChange={props.handleChange}
              style={{ width: '65px' }}
              type="number"
              name=""
              defaultValue={props.millisecondsBetweenRedraws}
              min="1"
              step="1"
            />
            ms
          </div>
          <div>
            <span>Scale amplitude: </span>
            <input
              onChange={props.handleChange}
              style={{ width: '65px' }}
              type="number"
              name=""
              defaultValue={props.millisecondsBetweenRedraws}
              min="1"
              step="1"
            />
            %
          </div>
          <span>Update scope every</span>
          <input
            onChange={props.handleChange}
            style={{ width: '65px' }}
            type="number"
            name=""
            defaultValue={props.millisecondsBetweenRedraws}
            min="1"
            step="1"
          />
          ms
        </div>
      )
  );
