import _ from 'lodash';
import { connect } from 'react-redux';
import { TimeDomainVisualizersDumb } from './TimeDomainVisualizersDumb';
import { nodes as nodeModels } from '../../AudioSystem';
import {
  memoizedStateQueries,
} from '../../redux';

const mapStateToProps =
  (state) => {
    const type = 'TimeDomainAnalyser';
    let timeDomainAnalyserNodes =
      Object.values(memoizedStateQueries.getNodesByType(state, type));
    timeDomainAnalyserNodes =
      _.cloneDeep(timeDomainAnalyserNodes); // todo. this is suboptimal.
    timeDomainAnalyserNodes.forEach((node) => {
      node.webAudioNode = nodeModels.nodes[node.id]; // eslint-disable-line
      // todo "node.webAudioNode" should be "node.nodeModel" because that is what it is.
    });
    return {
      /**
       * Get all TimeDomainAnalyser nodes.
       */
      timeDomainAnalyserNodes,
    };
  };


export const TimeDomainVisualizersRedux =
  connect(
    mapStateToProps,
    {},
  )(TimeDomainVisualizersDumb);
