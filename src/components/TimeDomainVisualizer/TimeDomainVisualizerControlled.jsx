import React from 'react';
import { TimeDomainVisualizerDumb } from './TimeDomainVisualizerDumb';

export const TimeDomainVisualizerControlled =
  (props) => {
    if (props.analyserNode !== null && props.analyserNode !== undefined) {
      return (
        <TimeDomainVisualizerDumb
          {...props}
        />
      );
    }
    return null;
  };
