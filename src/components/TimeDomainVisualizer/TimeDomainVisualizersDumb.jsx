import React from 'react';
import {
  TimeDomainVisualizerControlled,
} from './TimeDomainVisualizerControlled';
import {
  SimpleWindowRedux,
} from '../SimpleWindow';

export const TimeDomainVisualizersDumb =
  (props) => {
    const analysers =
      props.timeDomainAnalyserNodes.map(node => (
        <TimeDomainVisualizerControlled
          height="80"
          width="350"
          millisecondsBetweenRedraws={40}
          analyserNode={node}
        />
      ));
    return (
      <SimpleWindowRedux title="Time-Domain Analysers">
        {analysers}
      </SimpleWindowRedux>
    );
  };
