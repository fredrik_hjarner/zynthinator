import React from 'react';
import {
  InfiniteTimeDomainVisualizer,
} from '../../AudioSystem';
import { SimpleWindowRedux } from '../SimpleWindow';
import {
  TimeDomainVisualizerSettingsDumb,
} from './TimeDomainVisualizerSettingsDumb';

export class TimeDomainVisualizerDumb extends React.Component {
  componentDidMount() {
    this.visualizer = new InfiniteTimeDomainVisualizer();
    this.visualizer.init(
      this.props.analyserNode.webAudioNode.webAudioNode,
      this.timeDomainCanvasId, // todo. should not have a static Id that makes it IMPOSSIBLE to instantiate several TimeDomainVisualizer:s.
    );
    this.visualizer.setMillisecondsBetweenRedraws(this.props.millisecondsBetweenRedraws);
  }

  componentWillUnmount =
    () => {
      this.visualizer.destructor();
    }

  timeDomainCanvasId =
    `timeDomainCanvas${this.props.analyserNode.id}`;

  handlers = {
    handleChange:
      (e) => {
        this.visualizer.setMillisecondsBetweenRedraws(e.target.value);
      },
  }

  render() {
    return (
      <SimpleWindowRedux title={`id${this.props.analyserNode.webAudioNode.id}`}>
        <canvas
          style={{ display: 'block' }}
          id={this.timeDomainCanvasId}
          width={this.props.width}
          height={this.props.height}
        />
        <TimeDomainVisualizerSettingsDumb
          {...this.handlers}
          millisecondsBetweenRedraws={this.props.millisecondsBetweenRedraws}
        />
      </SimpleWindowRedux>
    );
  }
}
