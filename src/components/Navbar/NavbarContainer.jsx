import { connect } from 'react-redux';
import { Navbar } from './Navbar';

export const NavbarContainer =
  connect(
    null,
    null,
  )(Navbar);
