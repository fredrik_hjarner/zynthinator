import _ from 'lodash';
import { connect } from 'react-redux';
import { FrequencyDomainVisualizersDumb } from './FrequencyDomainVisualizersDumb';
import { nodes as nodeModels } from '../../AudioSystem';
import {
  memoizedStateQueries,
} from '../../redux';

const mapStateToProps =
  (state) => {
    const type = 'FrequencyDomainAnalyser';
    let frequencyDomainAnalyserNodes =
      Object.values(memoizedStateQueries.getNodesByType(state, type));
    frequencyDomainAnalyserNodes =
      _.cloneDeep(frequencyDomainAnalyserNodes); // todo. this is suboptimal.
    frequencyDomainAnalyserNodes.forEach((node) => {
      node.webAudioNode = // eslint-disable-line
        nodeModels.nodes[node.id]; // todo there is no guarantee that the node exists yet!!!
      // todo "node.webAudioNode" should be "node.nodeModel" because that is what it is.
    });
    return {
      /**
       * Get all FrequencyDomainAnalyser nodes.
       */
      frequencyDomainAnalyserNodes,
    };
  };


export const FrequencyDomainVisualizersRedux =
  connect(
    mapStateToProps,
    {},
  )(FrequencyDomainVisualizersDumb);
