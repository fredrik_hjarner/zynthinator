import React from 'react';
import {
  FrequencyDomainVisualizerControlled,
} from './FrequencyDomainVisualizerControlled';
import {
  SimpleWindowRedux,
} from '../SimpleWindow';

export const FrequencyDomainVisualizersDumb =
  (props) => {
    const analysers =
      props.frequencyDomainAnalyserNodes.map(node => (
        <FrequencyDomainVisualizerControlled
          height="80"
          width="350"
          millisecondsBetweenRedraws={40}
          analyserNode={node}
        />
      ));
    return (
      <SimpleWindowRedux title="Frequency-Domain Analysers">
        {analysers}
      </SimpleWindowRedux>
    );
  };
