import React from 'react';
import {
// Accordion,
// Icon,
} from 'semantic-ui-react';

export const FrequencyDomainVisualizerSettingsDumb =
  props => (
    props.settingsMinimized ?
      null :
      (
        <div>
          <div>Maximum frequency in FFT: {props.maxFrequency} Hz</div>
          <div>
            <span>Update scope every</span>
            <input
              onChange={props.handleChange}
              style={{ width: '65px' }}
              type="number"
              name=""
              defaultValue={props.millisecondsBetweenRedraws}
              min="1"
              step="1"
            />
            milliseconds
          </div>
        </div>
      )
  );
