import React from 'react';
import { FrequencyDomainVisualizerDumb } from './FrequencyDomainVisualizerDumb';

export const FrequencyDomainVisualizerControlled =
  props => (
    <FrequencyDomainVisualizerDumb
      {...props}
    />
  );
