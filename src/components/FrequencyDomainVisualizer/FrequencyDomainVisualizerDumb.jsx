import React from 'react';
import {
// Accordion,
// Icon,
} from 'semantic-ui-react';
import {
  calculateFrequencyInNthBin,
  InfiniteFrequencyDomainVizualizer,
} from '../../AudioSystem';
import { SimpleWindowRedux } from '../SimpleWindow';
import {
  FrequencyDomainVisualizerSettingsDumb,
} from './FrequencyDomainVisualizerSettingsDumb';

export class FrequencyDomainVisualizerDumb extends React.Component {
  componentDidMount() {
    this.visualizer = new InfiniteFrequencyDomainVizualizer();
    this.visualizer.init(
      this.props.analyserNode.webAudioNode.webAudioNode,
      this.frequencyDomainCanvasId,
    );
    this.visualizer.setMillisecondsBetweenRedraws(this.props.millisecondsBetweenRedraws);
  }

  componentWillUnmount =
    () => {
      this.visualizer.destructor();
    }

  frequencyDomainCanvasId =
    `frequencyDomainCanvas${this.props.analyserNode.id}`;

  handlers = {
    handleChange:
      (e) => {
        this.visualizer.setMillisecondsBetweenRedraws(e.target.value);
      },
  }

  render() {
    const bins = this.props.analyserNode.webAudioNode.webAudioNode.fftSize / 2;
    const maxFrequency = calculateFrequencyInNthBin(bins, this.props.analyserNode.webAudioNode.webAudioNode.fftSize);

    return (
      <SimpleWindowRedux title={`id${this.props.analyserNode.webAudioNode.id}`}>
        <canvas
          style={{ display: 'block' }}
          id={this.frequencyDomainCanvasId}
          width={this.props.width}
          height={this.props.height}
        />
        <FrequencyDomainVisualizerSettingsDumb
          maxFrequency={maxFrequency}
          {...this.handlers}
          millisecondsBetweenRedraws={this.props.millisecondsBetweenRedraws}
        />
      </SimpleWindowRedux>
    );
  }
}
