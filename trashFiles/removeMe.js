Immutable.setIn(
  state,
  ['nodeManagement', 'highestConnectionIdYet'],
  connectionId,
);

R.assocPath(
  ['nodeManagement', 'highestConnectionIdYet'],
  connectionId,
  state,
);

Immutable.withoutIn(
  state,
  ['nodeManagement', 'connections'],
  action.connectionId,
);

R.dissocPath(
  ['nodeManagement', 'connections', `${action.connectionId}`],
  state,
);

Immutable.withoutIn(
  newState,
  ['nodeManagement', 'connections'],
  conn =>
    conn.parentNodeId === action.nodeId ||
    conn.childNodeId === action.nodeId,
);

const _rejectPath =
  (path, predicate, state) =>
    R.compose(
      R.assocPath(path),
      R.reject(predicate),
      R.path(path),
    );

rejectPath =
  R.curry(this._withoutIn);

withoutIn(
  ['nodeManagement', 'connections'],
  conn =>
    conn.parentNodeId === action.nodeId ||
    conn.childNodeId === action.nodeId,
  state,
);

withoutIn(
  ['nodeManagement', 'connections'],
  R.either(
    R.propEq('parentNodeId', action.nodeId),
    R.propEq('childNodeId', action.nodeId),
  ),
  state,
);

/**
  appendPath(
    ['path-to', 'some', 'array'],
    valueToAppend,
    state,
  ),
 */
const appendPath =
  (path, value) =>
    R.compose(
      R.assocPath(path),
      R.append(value),
      R.path(path),
    );

/**
 * Refactoring
 */

const _transformPath =
  (transformer, path, state) =>
    R.compose(
      R.assocPath(path),
      transformer,
      R.path(path),
    )(state);

const transformPath =
  R.curry(_transformPath);

const rejectPath =
  (predicate, path, state) =>
    transformPath(
      R.reject(predicate),
      path,
      state,
    );

const rejectPath =
  R.compose(
    transformPath,
    R.reject,
  );

/**
  appendPath(
    valueToAppend,
    ['path-to', 'some', 'array'],
    state,
  ),
 */
const appendPath =
  R.compose(
    transformPath,
    R.append,
  );
