import * as R from 'ramda';

R.evolve({
  nodeManagement: {
    highestKnobIdYet:
      () => knobId,
    knobs:
      R.assocPath(`${knobId}`, knob),
  },
});

R.compose(
  R.evolve,
  knobId => ({
    nodeManagement: {
      highestKnobIdYet:
        () => knobId,
      knobs:
        R.assocPath(`${knobId}`, knob),
    },
  }),
);

R.converge(
  R.call,
  [
    R.compose(R.prop('indent'), indentN),
    R.always,
    R.toString,
  ],
)(knobId);
